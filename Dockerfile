FROM openjdk:8-jdk-alpine

ADD target/lab331-backend-dara.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom",\
    "-jar","/app.jar"]
